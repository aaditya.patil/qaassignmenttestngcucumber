package stepDefination;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features="src/test/resources"
,glue={"stepDefination"},plugin={"json:target/Reports/cucumber.json",
"html:target/Reports/report.html"})

public class TestRunner2 extends AbstractTestNGCucumberTests {

}
